
# I. Ansible first steps
![alt text](image.png)

on créer un vagrant avec python3 et ansible d'installer avec ces commande :

```
sudo dnf install python3 
sudo dnf install ansible -y
```
puis un repackage pour garder les installations faite : 

```
vagrant package --output ansible.box
vagrant box add ansible ansible.box
vagrant box list
```
![alt text](image-1.png)

```
python -m pip install ansible
```
Vue que WINDOWS n'est pas copatible avec quelque module alors nous sommes obligé de faire avec wsl, Alors on l'installe.

```
#Dans un terminal windows
wsl --install
wsl -s ubuntu
wsl
```

```
sudo apt update
sudo apt upgrade 
sudo apt install ansible -y
```
vous aurez peut être un problème avec windows de se type

```
exstander@exstander:~/ansible$ ansible -i hosts.ini tp1 -m ping
10.4.1.11 | UNREACHABLE! => {
    "changed": false,
    "msg": "Failed to connect to the host via ssh: toto@10.4.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).",
    "unreachable": true
}
10.4.1.12 | UNREACHABLE! => {
    "changed": false,
    "msg": "Failed to connect to the host via ssh: toto@10.4.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).",
    "unreachable": true
}
```

il suffira de copier et coller votre fichier ansible dans un fichier que votre wsl pourra lire en dehors de windows avec votre clef priver dans la même partie (ATTENTION NE PAS PARTARGER SA CLEF PRIVER)

```
cp /mnt/c/Users/romai/OneDrive/Bureau/ansible/.ssh-config  .
```
et vous pouvez ensuite utiliser les commandes ci-dessous :

```
# lister les hôtes que Ansible voit dans notre inventaire
$ ansible -i hosts.ini tp1 --list-hosts

# tester si ansible est capable d'interagir avec les machines
$ ansible -i hosts.ini tp1 -m ping

# afficher toutes les infos que Ansible est capable de récupérer sur chaque machine
$ ansible -i hosts.ini tp1 -m setup

# exécuter une commande sur les machines distantes
$ ansible -i hosts.ini tp1 -m command -a 'uptime'

# exécuter une commande en root
$ ansible -i hosts.ini tp1 --become -m command -a 'reboot'

```

Création du fichier YML
et teste du premier code ansible

```
#CONFIGURATION YML
---
- name: Install nginx
  hosts: tp1
  become: true

  tasks:
  - name: Install nginx
    dnf:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started


  - name: Open HTTP default port (80)*
    firewalld:
      port: 80/tcp
      state: enabled
      permanent: true
      immediate: true

ansible-playbook first.yml  -i hosts.ini -K

curl 10.4.1.11

"Hello from 10.0.2.15"
```
Mariadb yml

```
---
- name: Install Nginx and Configure Website
  hosts: db
  become: true

  tasks:
    - name: Install Mariadb package (assuming RedHat-based distro)
      dnf:
        name: mariadb
        state: latest
        update_cache: yes
```


# II. Range ta chambre

➜ Ajout d'un fichier de config Ansible

vim ansible.cfg

[defaults]
roles_path = ./roles


➜ Dans votre répertoire de travail Ansible...

mkdir roles roles/common roles/common/tasks | touch main.yml

vim roles/common/tasks/main.yml

```
- name: Install common packages
  import_tasks: packages.yml
- name: Users
  import_tasks: users.yml
```

touch roles/common/tasks/packages.yml | vim roles/common/tasks/packages.yml
```
- name: Install common packages
  ansible.builtin.package:
    name: "{{ item }}"
    state: present
  with_items: "{{ common_packages }}" # ceci permet de boucler sur la liste common_packages
```


mkdir roles/common/defaults/

touch roles/common/defaults/main.yml 

```
common_packages:
  - vim
  - git
```


mkdir playbooks | touch playbooks/main.yml

```
- hosts: tp4
  roles:
    - common
```

➜ Testez d'appliquer ce playbook avec une commande ansible-playbook

ansible-playbook -i /iventories/vagrant_lab/hosts.ini playbooks/main.yml -K

3.Structure du dépôt : variables d'inventaire

➜ Dans votre répertoire de travail Ansible...

mkdir inventories/vagrant_lab/host_vars/

touch inventories/vagrant_lab/host_vars/10.4.1.11.yml :
```
common_packages:
  - vim
  - git
  - rsync
```
➜ Dans votre répertoire de travail Ansible...

mkdir inventories/vagrant_lab/group_vars/

touch inventories/vagrant_lab/group_vars/tp4.yml :
```
users:
  - le_nain
  - l_elfe
  - le_ranger
```

➜ Modifiez le fichier roles/common/tasks/main.yml pour inclure un nouveau fichier  roles/common/tasks/users.yml :

touch roles/common/tasks/users.yml
vim roles/common/tasks/users.yml

```
- name: Add users from group vars
  ansible.builtin.user:
    name: "{{ item }}"
    comment: "Le user {{ item }}"
  with_items: "{{ users }}"
```

➜ Vérifiez la bonne exécution du playbook

ansible-playbook -K -i inventories/vagrant_lab/hosts.ini        playbooks/main.yml

➜ Créez un nouveau rôle nginx

mkdir roles/nginx/ roles/nginx/tasks/

vim roles/nginx/tasks/main.yml

```
- name: Install NGINX
  import_tasks: install.yml

- name: Configure NGINX
  import_tasks: config.yml

- name: Deploy VirtualHosts
  import_tasks: vhosts.yml
```

➜ Remplissez le fichier roles/nginx/tasks/install.yml

```
- name: Install nginx
  ansible.builtin.nginx:
    name: nginx
    state: present
```


➜ On va y ajouter quelques mécaniques : fichiers et templates :

mkdir roles/nginx/files/

touch roles/nginx/files/nginx.conf

récupérez un fichier nginx.conf par défaut (en faisant une install à la main par exemple)
ajoutez une ligne include conf.d/*.conf;



créez un répertoire roles/nginx/templates/

créez un fichier roles/nginx/templates/vhost.conf.j2 :


.j2 c'pour Jinja2, c'est le nom du moteur de templating utilisé par Ansible




server {
        listen {{ nginx_port }} ;
        server_name {{ nginx_servername }};

        location / {
            root {{ nginx_webroot }};
            index index.html;
        }
}


➜ Remplissez le fichier roles/nginx/tasks/config.yml :

- name : Main NGINX config file
  copy:
    src: nginx.conf # pas besoin de préciser de path, il sait qu'il doit chercher dans le dossier files/
    dest: /etc/nginx/nginx.conf


➜ Quelques variables roles/nginx/defaults/main.yml :

nginx_servername: test
nginx_port: 8080
nginx_webroot: /var/www/html/test
nginx_index_content: "<h1>teeeeeest</h1>"


➜ Remplissez le fichier roles/nginx/tasks/vhosts.yml :

- name: Create webroot
  file:
    path: "{{ nginx_webroot }}"
    state: directory

- name: Create index
  copy:
    dest: "{{ nginx_webroot }}/index.html"
    content: "{{ nginx_index_content }}"

- name: NGINX Virtual Host
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ nginx_servername }}.conf
# III. Repeat

Ptite section pour vous faire écrire un peu de code Ansible stylé.

➜ **[Document dédié à la Partie III. Repeat](./repeat.md)**

![Automate att](./img/automate_att.png)

# IV. Bonus : Aller plus loin

## 1. Vault Ansible

Afin de ne pas stocker de données sensibles en clair dans les fichiers Ansible, comme des mots de passe, on peut utiliser les [vault Ansible](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

Cela permet de stocker ces données, mais dans des fichiers chiffrés, à l'intérieur du dépôt Ansible.

➜ **Utilisez les Vaults pour stocker les clés publiques des utilisateurs**

## 2. Support de plusieurs OS

**Il est possible qu'un rôle donné fonctionne pour plusieurs OS.** Pour ça, on va utiliser des conditions en fonction de l'OS de la machine de destination.

A chaque fois qu'on déploie de la conf sur une machine, cette dernière nous donne beaucoup d'informations à son sujet : ses ***facts***. Par exemple, on récupère la liste des cartes réseau de la machine, la liste des utilisateurs, l'OS utilisé, etc.

On peut alors récupérer ces variables dans nos tasks, pour les insérer dans des templates par exemple, ou encore effectuer du travail conditionnel :

```yml
  - name: Install apache
    apt: 
      name: apache
      state: latest
    when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

  - name: Install apache
    yum: 
      name: httpd # le nom du paquet est différent sous CentOS
      state: latest
    when: ansible_distribution == 'CentOS'
```

➜ **Ajoutez une machine d'un OS différent à votre `Vagrantfile` et adaptez vos playbooks**

- passez sur une CentOS si vous étiez sur une base Debian jusqu'alors
- ou vice-versa
